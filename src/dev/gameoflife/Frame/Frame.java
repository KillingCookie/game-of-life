package dev.gameoflife.Frame;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Frame {
	
	private JPanel panel;
	private JFrame frame;
	private Canvas canvas;
	
	private String frame_title;
	private int frame_width, frame_height;
	
	public Frame(String frame_title, int frame_width, int frame_height){
		
		this.frame_title = frame_title;
		this.frame_width = frame_width;
		this.frame_height = frame_height;
		
		createFrame();
		
	}
	
	private void createFrame(){
		
		panel = new JPanel();
		panel.setPreferredSize(new Dimension(frame_width, 51));
		panel.setMaximumSize(new Dimension(frame_width, 51));
		panel.setMinimumSize(new Dimension(frame_width, 51));
		
		frame = new JFrame(frame_title);
		frame.setSize(frame_width, frame_height);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		
		canvas = new Canvas();
		canvas.setPreferredSize(new Dimension(frame_width, frame_height - 51));
		canvas.setMaximumSize(new Dimension(frame_width, frame_height - 51));
		canvas.setMinimumSize(new Dimension(frame_width, frame_height - 51));
		canvas.setFocusable(false);
		
		
		frame.add(canvas, BorderLayout.NORTH);
		frame.add(panel, BorderLayout.SOUTH);
		frame.pack();
		
	}
	
	public Canvas getCanvas(){
		
		return canvas;
		
	}
	
	public JFrame getFrame(){
		
		return frame;
		
	}
	
	public JPanel getPanel() {
		
		return panel;
		
	}
	
}