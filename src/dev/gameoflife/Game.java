package dev.gameoflife;

import java.awt.Graphics;
import java.awt.image.BufferStrategy;

import dev.gameoflife.Frame.Frame;
import dev.gameoflife.Input.Mouse;
import dev.gameoflife.States.GameState;
import dev.gameoflife.States.State;
import dev.gameoflife.gfx.Assets;

public class Game implements Runnable {

	private Frame gui;
	
	private Thread thread;
	private float tps = 60.0f;//ticks per second
	
	private boolean Run = false;
	private int frame_width, frame_height;
	//private String frame_title;
	
	private BufferStrategy BufStr;
	private Graphics g;
	
	private Mouse mouse;
	
	public State gameState;
	
	public Game(String frame_title, int frame_width, int frame_height){
		
		this.frame_width = frame_width;
		this.frame_height = frame_height;
		//this.frame_title = frame_title;
		mouse = new Mouse();
	//}
	
	//private void init(){
		
		gui = new Frame(frame_title, frame_width, frame_height);
		
		gui.getFrame().addMouseListener(mouse);
		gui.getFrame().addMouseMotionListener(mouse);
		gui.getCanvas().addMouseListener(mouse);
		gui.getCanvas().addMouseMotionListener(mouse);
		
		Assets.init();
		
		gameState = new GameState(this);
		State.setState(gameState);
		
	}

	private void tick(){
		
		if(State.getState() != null){
			
			State.getState().tick();
			
		}
		
	}
	
	private void render(){
		
		BufStr = gui.getCanvas().getBufferStrategy();
		if(BufStr == null){
			gui.getCanvas().createBufferStrategy(3);
			return;
		}
		
		g = BufStr.getDrawGraphics();
		
		g.clearRect(0, 0, frame_width, frame_height); //clear screen
		//
		
		if(State.getState() != null){
			
			State.getState().render(g);
			
		}
		
		//
		BufStr.show();
		g.dispose();
		
	}
	
	public void run(){
		
		//init();
		
		//int /*tps = 30,*/ fps = 0;
		double timePerTick = 1000000000 / tps, delta = 0, fpsDelta = 0, timePerRender = 1000000000/60;
		long now, lastTime = System.nanoTime();
		
		while(Run){
			
			timePerTick = 1000000000 / tps;
			now = System.nanoTime();
			delta += (now - lastTime) / timePerTick;
			fpsDelta += (now - lastTime) / timePerRender;
			lastTime = now;
			
			if(delta >= 1){
				
				tick();
				//render();//render always 60
				delta--;
				
			}if(fpsDelta >= 1) {
				
				render();
				fpsDelta--;
				
			}
		
		} 
		
		stop();
		
	}
	
	public Frame getFrame(){
		
		return gui;
		
	}

	public Mouse getMouse() {
		
		return mouse;
		
	}
	
	public float getTicksPerSecond() {
		
		return tps;
		
	}
	
	public void setTicksPerSecond(float tps) {
		
		this.tps = tps;
		
	}
	
	public synchronized void start(){
		
		if(Run){
			return;
		}else{
			Run = true;
			thread = new Thread(this);
			thread.start();
			
		}
		
	}

	public synchronized void stop(){
		
		if(!Run){
			return;
		}else{
			Run = false;
			try {
				thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
		
	}
	
}
