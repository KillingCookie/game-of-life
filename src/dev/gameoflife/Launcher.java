package dev.gameoflife;

public class Launcher {

	public static void main(String[] args){
		
		Game Game = new Game("Game of Life", 1024, 768);
		Game.start();
		
	}
	
}
