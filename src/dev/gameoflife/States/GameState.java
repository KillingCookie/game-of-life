package dev.gameoflife.States;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Hashtable;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import dev.gameoflife.Game;
import dev.gameoflife.gfx.Assets;

public class GameState extends State {
	
	private float scale = 12.0f;
	private float speed = 2.0f;
	private boolean pause = true;
	private boolean[][] field = new boolean[98][130];
	
	private JButton bStart, bNextFrame, bClear;
	private JSlider sTime, sZoom;
	private JLabel lTime, lZoom;

	public GameState(Game game){
		
		super(game);
		
		lTime = new JLabel("speed(sec)");
		lTime.setFont(lTime.getFont().deriveFont(10.0f));
		lZoom = new JLabel("scale(px)");
		lZoom.setFont(lZoom.getFont().deriveFont(10.0f));
		
		bStart = new JButton("Start");
		bNextFrame = new JButton("Next Frame");
		bClear = new JButton("Clear");
		
		sTime = new JSlider(1, 10);
		sZoom = new JSlider(8, 32, 16);
		sTime.setMinorTickSpacing(1);
		sZoom.setMinorTickSpacing(4);
		sTime.setPaintTicks(true);
		sZoom.setPaintTicks(true);
		sTime.setSnapToTicks(true);
		sZoom.setSnapToTicks(true);
		
		Hashtable<Integer, JLabel> hTime, hZoom;
		hTime = new Hashtable<Integer, JLabel>();
		hZoom = new Hashtable<Integer, JLabel>();
		
		JLabel l = new JLabel("1");
		l.setFont(l.getFont().deriveFont(10.0f));
		hTime.put(10, l);
		//hTime.put(9, new JLabel("0.9"));
		//hTime.put(8, new JLabel("0.8"));
		//hTime.put(7, new JLabel("0.7"));
		//hTime.put(6, new JLabel("0.6"));
		l = new JLabel("0.5");
		l.setFont(l.getFont().deriveFont(10.0f));
		hTime.put(5, l);
		//hTime.put(4, new JLabel("0.4"));
		//hTime.put(3, new JLabel("0.3"));
		//hTime.put(2, new JLabel("0.2"));
		l = new JLabel("0.1");
		l.setFont(l.getFont().deriveFont(10.0f));
		hTime.put(1, l);
		sTime.setLabelTable(hTime);
		sTime.setPaintLabels(true);
		
		l = new JLabel("8");
		l.setFont(l.getFont().deriveFont(10.0f));
		hZoom.put(8, l);
		l = new JLabel("16");
		l.setFont(l.getFont().deriveFont(10.0f));
		hZoom.put(16, l);
		l = new JLabel("24");
		l.setFont(l.getFont().deriveFont(10.0f));
		hZoom.put(24, l);
		l = new JLabel("32");
		l.setFont(l.getFont().deriveFont(10.0f));
		hZoom.put(32, l);
		sZoom.setLabelTable(hZoom);
		sZoom.setPaintLabels(true);
		
		bStart.setBounds(30 , 7, 150, 37);
		bNextFrame.setBounds(210 , 7, 150, 37);
		bClear.setBounds(390 , 7, 150, 37);
		
		lTime.setBounds(570 + 180/2 - 30, 0, 60, 10);
		lZoom.setBounds(790 + 180/2 - 30, 0, 60, 10);
		
		sTime.setBounds(570, 13, 180, 51 - 13);
		sZoom.setBounds(790, 13, 180, 51 - 13);
		
		bStart.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				if(pause) {
					pause = false;
					game.setTicksPerSecond(speed);
					bStart.setText("Stop");
					
				}else {
					pause = true;
					game.setTicksPerSecond(60.0f);
					bStart.setText("Start");
					
				}
				
			}
			
		});
		
		bNextFrame.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				field = nextInstance();
				
			}
			
		});
		
		bClear.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				for(int i = 0; i <= 129; i++){
					for(int j = 0; j <= 97; j++){
						field[j][i] = false;
					}
					
				}if(!pause) { 
					pause = true;
					game.setTicksPerSecond(60.0f);
					bStart.setText("Start");
				}
				
			}
			
		});
		
		sTime.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				
				speed = 10.0f/(float)sTime.getValue();
				if(!pause) game.setTicksPerSecond(speed);//in tick if pause/no
				
			}
			
		});
		
		sZoom.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				
				scale = sZoom.getValue();
				
			}
			
		});
		
		game.getFrame().getPanel().setLayout(null);
		game.getFrame().getPanel().add(bStart);
		game.getFrame().getPanel().add(bNextFrame);
		game.getFrame().getPanel().add(bClear);
		game.getFrame().getPanel().add(sTime);
		game.getFrame().getPanel().add(sZoom);
		game.getFrame().getPanel().add(lTime);
		game.getFrame().getPanel().add(lZoom);
		game.getFrame().getFrame().revalidate();
		
	}
	
	@Override
	public void tick() {//pause = 60 tps
		
		if(pause) {
			
			float xWidth, yHeight;
			xWidth = 128/(scale/8);
			yHeight = 96/(scale/8);
			
			int x, y;
		
			//if(game.getMouse().isLeftPressed() && (x = game.getMouse().getX()) > 0 && (y = game.getMouse().getY()) > 0) {
			if(game.getMouse().isLeftPressed()){
				x = game.getMouse().getX(); 
				y = game.getMouse().getY();
			
				if(y < 768 - 51) {
					
					float xx, yy;
					xx = (float) Math.floor(x/scale + 128/2 - xWidth/2 + 1);
					yy = (float) Math.floor(y/scale + 96/2 - yHeight/2 + 1);
					
					if(field[(int) yy][(int) xx] == false)
						field[(int) yy][(int) xx] = true;
					else
						field[(int) yy][(int) xx] = false;
				
				}game.getMouse().unpressLeft();
			
			}
			if(game.getMouse().isRightPressed()) {
			
				
				
			}
		
		}else {
			
			field = nextInstance();
			
		}
		
	}
	
	private boolean[][] nextInstance(){
		
		boolean[][] nextInstance = new boolean[98][130];
		
		for(int y = 0; y < 98; y++) {
			for(int x = 0; x < 130; x++) {
				nextInstance[y][x] = this.field[y][x];
				
			}
		
		}
		
		for(int y = 1; y < 97; y++) {
			for(int x = 1; x < 129; x++) {
				
				int neighbours = (this.field[y-1][x-1]?1:0) + (this.field[y-1][x]?1:0) + (this.field[y-1][x+1]?1:0) +
									(this.field[y][x-1]?1:0) 				 +				 (this.field[y][x+1]?1:0) +
									(this.field[y+1][x-1]?1:0) + (this.field[y+1][x]?1:0) + (this.field[y+1][x+1]?1:0);
				
				if(!this.field[y][x] && (neighbours == 3)) {
					
					nextInstance[y][x] = true;
					
				}else if(this.field[y][x] && ((neighbours < 2) || (neighbours > 3))) {
					
					nextInstance[y][x] = false;
					
				}
				
			}
			
		}
		
		return nextInstance;
		
	}

	@Override
	public void render(Graphics g) {
		
		float xWidth, yHeight;
		xWidth = 128/(scale/8);
		yHeight = 96/(scale/8);
	
		for(int y = 1; y < 97; y++) {
			for(int x = 1; x < 129; x++) {//< 768-51
			
				if(field[y][x] == false && x - 1 >= Math.floor(128/2 - xWidth/2) && x-1 <= 128 - (128/2 - Math.floor(xWidth/2))
										&& y - 1 >= Math.floor(96/2 - yHeight/2) && y-1 <= 96 - (96/2 - Math.floor(yHeight)/2)) {
					
					g.drawImage(Assets.black, (int)(((x-1) - (128/2 - xWidth/2))*scale), (int)(((y-1) - (96/2 - yHeight/2))*scale), (int)scale, (int)scale, null);
				
				}else if((x - 1 >= 128/2 - xWidth/2 && x-1 <= 128 - (128/2 - xWidth/2)
						&& y - 1 >= 96/2 - yHeight/2 && y-1 <= 96 - (96/2 - yHeight/2))) {
					
					g.drawImage(Assets.white, (int)(((x-1) - (128/2 - xWidth/2))*scale), (int)(((y-1) - (96/2 - yHeight/2))*scale), (int)scale, (int)scale, null);
					
				}
				
			}
			
		}
		
		//small taskbar
		/*g.setColor(new Color(167, 167, 167));
		g.fillRect(0, 768 - 51, 1024, 51);*/
		game.getFrame().getFrame().repaint();
		
	}
	
}
