package dev.gameoflife.gfx;

import java.awt.image.BufferedImage;

public class Assets {

	//private static final int width = 32, height = 32;
	
	public static BufferedImage black, white;
	
	public static void init(){
		
		black = TextureLoader.LoadImg("/black.png");
		white = TextureLoader.LoadImg("/white.png");
		
	}
		
}
